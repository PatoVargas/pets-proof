from django.contrib import admin

from .models import Photo, User, Vote

# Register your models to admin site, then you can add, edit, delete and search your models in Django admin site.
admin.site.register(Photo)
admin.site.register(User)
admin.site.register(Vote)
