from django.shortcuts import render

from django.http import HttpResponse

from django.db.models import Count
from django.views.generic.base import TemplateView
from django.views import generic
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from .models import Photo, User, Vote

import json, datetime

class HomePageView(TemplateView):

    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['photos'] = Photo.objects.annotate(count=Count('vote__id')).order_by('-count')
        return context

home = HomePageView.as_view()

class CreatePhotoView(generic.View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CreatePhotoView, self).dispatch(request=request)

    def post(self, request):
        dic = {}
        nickname = self.request.POST['nickname']
        pet = self.request.POST['pet']
        ffile = self.request.FILES['a_file']
        user = User.objects.filter(nickname = nickname)
        if user.exists():
            user = user.first()
        else:
            user = User.objects.create(nickname = nickname)
        Photo.objects.create(user = user, name = pet, file = ffile)
        dic['success'] = True
        return HttpResponse(content=json.dumps(dic), content_type='application/json; charset=utf-8')

create_photo = CreatePhotoView.as_view()


class VotePhotoView(generic.View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(VotePhotoView, self).dispatch(request=request)

    def post(self, request):
        dic = {}
        nickname = self.request.POST['nickname']
        id = self.request.POST['id']
        photo = Photo.objects.get(id = id)
        user = User.objects.filter(nickname = nickname)
        if user.exists():
            user = user.first()
        else:
            user = User.objects.create(nickname = nickname)
        vote = Vote.objects.filter(user = user, photo = photo)
        if vote.exists():
            dic['success'] = False
        else:
            vote = Vote.objects.create(user = user, photo = photo)
            dic['success'] = True
        return HttpResponse(content=json.dumps(dic), content_type='application/json; charset=utf-8')

vote_photo = VotePhotoView.as_view()
