from django.db import models
from django.utils.translation import ugettext_lazy as _

from model_utils.models import TimeStampedModel

class Photo(TimeStampedModel):

    user = models.ForeignKey('User', verbose_name=_('User'), on_delete=models.CASCADE)
    name = models.CharField(verbose_name=_('Name'), max_length=100, blank=True)
    file = models.FileField(
        verbose_name=_('attachment'), upload_to='media/',
        blank=True, null=True
    )

    @property
    def name_file(self):
        return self.file.url.split('/')[-1]

class User(TimeStampedModel):
    nickname = models.CharField(verbose_name=_('Name'), max_length=100, blank=True)

class Vote(TimeStampedModel):
    user = models.ForeignKey('User', verbose_name=_('User'), on_delete=models.CASCADE)
    photo = models.ForeignKey('Photo', verbose_name=_('Photo'), on_delete=models.CASCADE)
